#!/bin/bash

#Automatiser la creation des security groups
microstack.openstack security group create VMs
microstack.openstack security group rule create --dst-port 22:22 --ingress --protocol tcp VMs
microstack.openstack security group rule create --dst-port 80:80 --ingress --protocol tcp VMs
microstack.openstack security group rule create --dst-port 443:443 --ingress --protocol tcp VMs
microstack.openstack security group rule create --dst-port 8000:8000 --ingress --protocol tcp VMs
microstack.openstack security group rule create --ingress --protocol tcp VMs
microstack.openstack security group rule create --ingress VMs
microstack.openstack security group rule create --egress --protocol icmp VMs
microstack.openstack security group rule create --egress VMs

#Automatiser la creation des flavors
microstack.openstack flavor create GabariMinimal --ram 1024 --disk 10 --vcpus 1

#Automatiser la creation des images
sudo apt install git -y
git clone https://gitlab.com/Camille2791/openstack.git
cd openstack
microstack.openstack image create Ubuntu_18.04 --disk-format qcow2 --file ./bionic-server-cloudimg-amd64.img

#Generation des clefs
echo "ubuntu" > password.txt
ssh-keygen -q -N ""  -f open_key_ubuntu
microstack.openstack keypair create --public-key open_key_ubuntu.pub openstack_key
microstack.openstack keypair list

#Generation des instances en les associant aux adresses IP
for machine in `echo vm1 vm2 vm3 vm4 vm5`; do

    echo "Debut creation VM $machine"

    case $machine in
     vm1)
       echo -n "vm1";

       #Creation d'une instance
       microstack.openstack server create --network test --key-name openstack_key --flavor GabaritMinimal --image ubuntu18.04 $machine
    
       #Creation d'une adresse IP
       microstack.openstack floating ip create external
       
       #Troncage de la ligne donnant des informations sur l'adresse IP à sa seule valeur
       foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

       if [ ! -z "$foo" ]; then
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante                                                                                                                                                     
          echo "Floating IP = $bar"
       else
          echo "Error"
          exit 1
       fi

       #Association de la valeur de l'IP à une variable reconnaissable en-dehors de la boucle
       ip_vm1=$bar ;;


     vm2)
       echo -n "vm2";

       microstack.openstack server create --network test --key-name openstack_key --flavor GabaritMinimal --image ubuntu18.04 $machine

       microstack.openstack floating ip create external
       foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

       if [ ! -z "$foo" ]; then
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante                                                                                                                                                     
          echo "Floating IP = $bar"
       else
          echo "Error"
          exit 1
       fi


       ip_vm2=$bar ;;

     vm3)
       echo -n "vm3";

       microstack.openstack server create --network test --key-name openstack_key --flavor GabaritMinimal --image ubuntu18.04 $machine
    
       microstack.openstack floating ip create external
       foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

       if [ ! -z "$foo" ]; then
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante                                                                                                                                                     
          echo "Floating IP = $bar"
       else
          echo "Error"
          exit 1
       fi

       ip_vm3=$bar ;;

     vm4)
       echo -n "vm4";
       
       microstack.openstack server create --network test --key-name openstack_key --flavor GabaritMinimal --image ubuntu18.04 $machine
    
       microstack.openstack floating ip create external
       foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

       if [ ! -z "$foo" ]; then
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante                                                                                                                                                     
          echo "Floating IP = $bar"
       else
          echo "Error"
          exit 1
       fi

       ip_vm4=$bar ;;

     vm5)
       echo -n "vm5";
	
       microstack.openstack server create --network test --key-name openstack_key --flavor GabaritMinimal --image ubuntu18.04 $machine
    
       microstack.openstack floating ip create external
       foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

       if [ ! -z "$foo" ]; then
        #$value= echo "$foo" | head -n1 | awk '{print $1;}' 
        bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
        # La variable bar contient l'adresse ip flottante                                                                                                                                                     
          echo "Floating IP = $bar"
       else
          echo "Error"
          exit 1
       fi

       ip_vm5=$bar ;;  

     *)
    echo -n "unknown" ;;
    esac

    #Association de l'adresse IP à l'instance
    microstack.openstack server add floating ip $machine $bar
    echo "Fin de la creation VM $machine"

done

echo "Je dors pendant 60s"
sleep 60

for machine in `echo vm1 vm2 vm3 vm4 vm5`; do
    # controle du demarrage instance
    # a=`microstack.openstack server list --name ${machine} --status ACTIVE`
    # echo "$a++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!!"
    # while [ -z "$a" ]; do
    #   echo "Please wait"
    #   sleep 1
    #   a=`microstack.openstack server list --name ${machine} --status ACTIVE`
    # done
 
    case $machine in
    vm1)
      echo -n "vm1";
      bar=$ip_vm1 ;
      #url="";;

    echo "ubuntu@$bar";
    sshpass -f password.txt ssh-copy-id  -i ./open_key_ubuntu.pub ubuntu@"$bar";
    sudo cat ./open_key_ubuntu.pub | ssh -f password.txt ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys";
    ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar;
#    ssh -i open_key_ubuntu ubuntu@$bar -y;; # avec quoi, on se connecte à la VM dans le Terminal et il faut y installer manuellement et sortir avec exit
    
    vm2)
      echo -n "vm2";
      bar=$ip_vm2 ;
      #url="";;

    echo "ubuntu@$bar";
    sshpass -f password.txt ssh-copy-id  -i ./open_key_ubuntu.pub ubuntu@"$bar";
    sudo cat ./open_key_ubuntu.pub | ssh -f password.txt ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys";
    ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar;; # sans quoi, il faut écrire Yes pour s'y connecter
#    ssh -i open_key_ubuntu ubuntu@$bar;;

    vm3)
      echo -n "vm3";
      bar=$ip_vm3 ;
      #url="";;

    echo "ubuntu@$bar";
    sshpass -f password.txt ssh-copy-id  -i ./open_key_ubuntu.pub ubuntu@"$bar";
    sudo cat ./open_key_ubuntu.pub | ssh -f password.txt ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys";
    ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar;;
#    ssh -i open_key_ubuntu ubuntu@$bar;

    vm4)
      echo -n "vm4";
      bar=$ip_vm4 ;
      #url="";;

    echo "ubuntu@$bar";
    sshpass -f password.txt ssh-copy-id  -i ./open_key_ubuntu.pub ubuntu@"$bar";
    sudo cat ./open_key_ubuntu.pub | ssh -f password.txt ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys";
    ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar;;
#    ssh -i open_key_ubuntu ubuntu@$bar;

    vm5)
      echo -n "vm5";
      bar=$ip_vm5 ;
      #url="";;

    echo "ubuntu@$bar";
    sshpass -f password.txt ssh-copy-id  -i ./open_key_ubuntu.pub ubuntu@"$bar";
    sudo cat ./open_key_ubuntu.pub | ssh -f password.txt ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys";
    ssh -o "StrictHostKeyChecking=no" -o PasswordAuthentication=no ubuntu@$bar;;
#    ssh -i open_key_ubuntu ubuntu@$bar;

    *)
    echo -n "unknown" ;;
    esac
done

sudo iptables -t nat -A POSTROUTING -s 10.20.20.1/24 ! -d 10.20.20.1/24 -j MASQUERADE
sudo sysctl net.ipv4.ip_forward=1
sudo chmod 400 open_key_ubuntu

for machine in `echo vm1 vm2 vm3 vm4 vm5`; do

    case $machine in

        vm1)
            echo "Installation Apache2"
            ssh -i open_key_ubuntu ubuntu@$ip_vm1 "sudo apt install git -y; git clone https://gitlab.com/Camille2791/openstack.git; cd Openstack; source ./openstack/Apache.sh";;

        vm2)
            echo "Installation Syncthing"
            ssh -i open_key_ubuntu ubuntu@$ip_vm2 "sudo apt install git -y; git clone https://gitlab.com/Camille2791/openstack.git; cd Openstack; source ./openstack/Syncthing_camille.sh";;

        vm3)
            echo "Installation Odoo"
            ssh -i open_key_ubuntu ubuntu@$ip_vm3 "sudo apt install git -y; git clone https://gitlab.com/Camille2791/openstack.git; cd Openstack; source ./openstack/script-install-odoo14.sh";;

        vm4)
            echo "Installation Nextcloud"
            ssh -i open_key_ubuntu ubuntu@$ip_vm4 "sudo apt install git -y; git clone https://gitlab.com/Camille2791/openstack.git; cd Openstack; source ./openstack/Nextcloud_camille.sh";;

        vm5)
            echo "Installation tests";;
            #ssh -i open_key_ubuntu ubuntu@$ip_vm5 "";;

        *)

          echo "Unknown" ;;

    esac

done

# Les scripts ne se lancent pas en même temps.
#    "sudo apt install git -y;
#    sudo git clone https://gitlab.com/Camille2791/openstack.git;
#    cd Openstack;
#    source Odoo.sh";;

