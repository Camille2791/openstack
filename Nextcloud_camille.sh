# Install Apache, MariaDB and PHP
sudo apt update
sudo apt-get install apache2 mariadb-server apache2 php7.2 php7.2-gd php7.2-json php7.2-mysql php7.2-curl php7.2-mbstring php7.2-intl php7.2-imagick php7.2-xml php7.2-zip libapache2-mod-php7.2 unzip wget -y
sudo touch /etc/php/7.2/apache2/php.ini
sudo chmod 557 /etc/php/7.2/apache2/php.ini
sudo echo "file_uploads = On
allow_url_fopen = On
memory_limit = 256M
upload_max_filesize = 100M
display_errors = Off
date.timezone = America/Chicago" > /etc/php/7.2/apache2/php.ini
sudo systemctl start apache2
sudo systemctl start mariadb
sudo systemctl enable apache2
sudo systemctl enable mariadb

# Configure Database
mysql -u root -p --execute "CREATE DATABASE nextclouddb;
CREATE USER 'nextclouduser'@'localhost' IDENTIFIED BY 'password';
GRANT ALL ON nextclouddb.* TO 'nextclouduser'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;;
EXIT;"

# Install NextCloud
sudo wget https://download.nextcloud.com/server/releases/nextcloud-16.0.3.zip
sudo unzip nextcloud-16.0.3.zip -d /var/www/html/
sudo chown -R www-data: /var/www/html/nextcloud

# Configure Apache for NextCloud
sudo touch /etc/apache2/sites-available/nextcloud.conf
sudo chmod 557 /etc/apache2/sites-available/nextcloud.conf
sudo echo "<VirtualHost *:8050>
ServerAdmin admin@example.com
DocumentRoot /var/www/html/nextcloud/
ServerName example.com
<Directory /var/www/html/nextcloud/>
Options +FollowSymlinks
AllowOverride All
Require all granted
<IfModule mod_dav.c>
Dav off
</IfModule>
SetEnv HOME /var/www/html/nextcloud
SetEnv HTTP_HOME /var/www/html/nextcloud
</Directory>
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" > /etc/apache2/sites-available/nextcloud.conf
sudo a2ensite nextcloud.conf
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
sudo chmod 577 /etc/apache2/ports.conf
sudo echo "Listen 8050" >> /etc/apache2/ports.conf
sudo systemctl restart apache2


